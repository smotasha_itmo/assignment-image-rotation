#include "bmp.h"
#include "close.h"
#include "open.h"
#include "open_read.h"
#include "rotate.h"


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; 
    if(argc!=3){
        fprintf(stderr,"Wrong amount of arguments");
        return -1;
    }

    const char *output_path = argv[2];
    FILE *output = NULL;

    struct image image = open_read(argv[1]);
    

    switch (open_for_writing(output_path,&output)) {
        case O_FAIL:
            fprintf(stderr,"You can't open output file");
            return -1;
        case OPEN:
            break;
    }
    struct image rotated = rotate(&image);
    enum write_status write_status = to_bmp(output, &rotated);
    if (write_status!=WRITE_OK) {
        fprintf(stderr, "You can't write into your file");
        return -1;
    }
    switch (close(&output)) {
        case C_FAIL:
            fprintf(stderr,"Output file hasn't been closed");
            return -1;
        case CLOSE:
            break;
    }
    free(image.data);
    free(rotated.data);

    return 0;

}

