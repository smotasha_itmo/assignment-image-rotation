#include "bmp.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>


struct pixel* get_pixel(struct image image, uint64_t j, uint64_t i){
    return &image.data[j * image.width + i];
}


struct image rotate(struct image* img){
    struct image image = {.width=img->height, .height=img->width};
    image.data = (struct pixel*) malloc(sizeof(struct pixel) * img->width * img->height);
    for (uint64_t i = 0; i < image.width; i++) {
        for (uint64_t j = 0; j < image.height; j++) {
            *(get_pixel(image, j, image.width - 1 - i))=*(get_pixel(*img, i, j));
        }
    }
    return image;
}
