#include "bmp.h"
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression; 
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

uint64_t calculate_padding(uint64_t width);

struct bmp_header create_header(const struct image * img);

struct image create_image(uint64_t height, uint64_t width);

enum read_status check_read( struct bmp_header header);

enum read_status from_bmp( FILE* in, struct image* img ){
    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in)!=1 || header.bfReserved !=0 || header.biPlanes != 1){
        return READ_FAILED;
    }
    enum read_status check = check_read(header);
    if (check!=READ_OK){
        return check;
    }
    *img= create_image((uint64_t)header.biHeight,(uint64_t)header.biWidth);
    
    if(img->height==0 || img->width==0 || img -> data==NULL){
        return INVALID_PICTURE;
    }

    uint64_t const padding = calculate_padding(img->width);
    for (int i = 0; i < img->height; i++) {
        if((fread(&(img->data[i * img->width]), sizeof (struct pixel), img->width, in)!=img->width ||
           fseek(in, (long) padding, SEEK_CUR)!=0)){
            img->height = 0;
            img->width = 0;
            free(img->data);
            return READ_FAILED;
        }
    }

    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ){
    if(out==NULL){
        return WRITE_ERROR;
    }
    if(img==NULL){
        return WRITE_ERROR;
    }
    uint64_t padding = calculate_padding(img->width);
    struct bmp_header header = create_header(img);

    if(fwrite(&header,sizeof(struct bmp_header),1,out)!=1){
        return WRITE_ERROR;
    }
    for(size_t i = 0;i< img->height;i++){
        if(fwrite((uint8_t *) img->data+i * img->width * 3, sizeof(struct pixel), img->width, out)!=img->width ||
           fseek(out, (long) padding, SEEK_CUR)!=0){
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;

}
uint64_t calculate_padding(uint64_t width){
    return 4 - ( ((width) * sizeof(struct pixel))%4 ) ;
}

struct image create_image(uint64_t height, uint64_t width){
    struct image img = {
            .width=width,
            .height=height,
            .data=(struct pixel*) malloc(sizeof(struct pixel) * width * height)
    };
    return img;
}


enum read_status check_read(struct bmp_header header){
    
    if (header.bfType !=0x4D42) { 
        return READ_INVALID_SIGNATURE; 
    }
    if (header.biSize!= 40) { 
        return READ_INVALID_HEADER; 
    }
    if (header.biBitCount != 24) { 
        return READ_INVALID_BITS; 
    }
    return READ_OK;
}

struct bmp_header create_header(const struct image * img){
    uint64_t const padding = calculate_padding(img->width);
    struct bmp_header header = {
            .bfType = 0x4D42,
            .bfileSize = img->height * img->width * sizeof(struct pixel) +  img->height * padding + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage =  img->height * img->width * sizeof(struct pixel) + padding*img->height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };
    return header;
}

