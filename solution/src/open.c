#include "open.h"
#include <stdint.h>

enum open_status open_for_writing (const char *file_path, FILE **file){
    *file = fopen(file_path,"wb");
    if (!file){
        return O_FAIL;
    }
    return OPEN;
}
enum open_status open_for_reading (const char *file_path, FILE **file){
    *file = fopen(file_path,"rb");
    if (!file){
        return O_FAIL;
    }
    return OPEN;
}

