#include "close.h"

enum close_status close(FILE **file){
    if (fclose(*file)==EOF) {
        return C_FAIL;
    }
    return CLOSE;

}

