#include "open.h"
#include "bmp.h"
#include "close.h"
#include "open_read.h"


#include <stdio.h>
#include <stdlib.h>

struct image open_read(char const *input_path){

    FILE *input = NULL;
    struct image empty = (struct image) {
            .width = 0,
            .height = 0,
            .data = NULL
    };
    switch (open_for_reading(input_path,&input)) {
        case O_FAIL:
            fprintf(stderr,"There is a problem with input file");
            return empty;
        case OPEN:
            break;
    }
    struct image image;

    enum read_status r_status = from_bmp(input, &image);
    switch (r_status){
    case READ_INVALID_BITS:
        fprintf(stderr,"You have invalid bits in your file");
        return empty;
    case READ_INVALID_HEADER:
        fprintf(stderr,"You have invalid header in your file");
        return empty;
    case READ_INVALID_SIGNATURE:
        fprintf(stderr,"You have invalid signature");

        return empty;
    case INVALID_PICTURE:
        fprintf(stderr,"You have invalid sizes of your picture");
        return empty;
    case READ_FAILED:
        fprintf(stderr,"Reading is failed");
        return empty;
    case READ_OK:
       
        break;
    }
    if (close(&input)==C_FAIL){
        fprintf(stderr,"Closing input is failed");
    }
    return image;
}
