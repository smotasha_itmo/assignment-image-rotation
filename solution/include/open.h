#ifndef OPEN_H_
#define OPEN_H_
#include <stdio.h>

enum open_status{
    OPEN,
    O_FAIL
};
enum open_status open_for_writing (const char *file_path, FILE **file);
enum open_status open_for_reading (const char *file_path, FILE **file);
#endif // OPEN_H_

