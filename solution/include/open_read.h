#ifndef OPEN_READ_H_
#define OPEN_READ_H_
#include <stdio.h>
#include <stdlib.h>

struct image open_read(char const*input_path);

#endif // OPEN_READ_H_
