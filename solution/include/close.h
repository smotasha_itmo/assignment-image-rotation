#ifndef CLOSE_H
#define CLOSE_H
#include <stdio.h>

enum close_status {
    C_FAIL,
    CLOSE
};
enum close_status close(FILE **file);
#endif // CLOSE_H

